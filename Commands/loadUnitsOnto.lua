function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load units onto a given transporter",
		parameterDefs = {
			{ 
				name = "units", -- which units to load
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "transporter", -- which transporter to use
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

local function ClearState(self)

end

function Run(self, allUnits, parameter)
	local units = parameter.units
	local transporter = parameter.transporter
	
	local allLoaded = true

	for i,unit in ipairs(units) do
		Spring.GiveOrderToUnit(unit, CMD.LOAD_ONTO, {transporter}, {});
		
		if Spring.GetUnitTransporter(unit) == nil then
			allLoaded = false
		end
	end

	if allLoaded == true then
		return SUCCESS
	else
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
