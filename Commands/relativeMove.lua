function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Simple relative move. Does not do any checks. Gives new move order in every AI frame.",
		parameterDefs = {
			{ 
				name = "direction", -- move direction
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "fight", -- whether to fight or not
				variableType = "expression",
				componentType = "checkBox",
				defaultValue = "false",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)

end

function Run(self, units, parameter)
	local direction = parameter.direction
	local fight = parameter.fight

	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	for i,unit in ipairs(units) do
		local pointX, pointY, pointZ = SpringGetUnitPosition(unit)
		local unitPosition = Vec3(pointX, pointY, pointZ)
		local targetPosition = unitPosition + direction
		
		SpringGiveOrderToUnit(unit, cmdID, targetPosition:AsSpringVector(), {})
	end

	return RUNNING
end


function Reset(self)
	ClearState(self)
end
