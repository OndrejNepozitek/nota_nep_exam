local sensorInfo = {
	name = "ReverseTable",
	desc = "Reverses a given table.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

return function(arr)
	local reversed = {}

	for i=1,#arr do
		reversed[#arr-i+1] = arr[i]
	end

	return reversed
end