local sensorInfo = {
	name = "GetAvailableFarck",
	desc = "Gets an available transporter and assigns a given branch.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local usedUnits = {}

return function()
    local units = Sensors.nota_sim_exam.GetFarcks()

	for i=1, #units do
		local unit = units[i]

		if ((usedUnits[unit] == nil) and Spring.ValidUnitID(unit) == true) then
			usedUnits[unit] = true
			return unit
		end
	end

	return nil
end