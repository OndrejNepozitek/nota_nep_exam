local sensorInfo = {
	name = "FirstUnitPosition",
	desc = "Returns position of the first unit.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return static position of the first unit
return function(listOfUnits)
	if (#listOfUnits == 0) then
		Logger.error("nep.FirstUnitPosition", "There must be at least one unit")
		return FAILURE
	end

	local x,y,z = SpringGetUnitPosition(listOfUnits[1])
	return Vec3(x,y,z)
end